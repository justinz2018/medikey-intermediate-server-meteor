if (Meteor.isClient) {
	jQuery.fn.rotate = function(deg) {
		$(this).css("transform", "rotate("+deg+"deg)");
		return $(this);
	};
	Template.main.events({
		"click .clickable": function(event) {
			var url = $(event.currentTarget).data("url");
			if(url) {
				Router.go(url);
			}
		},
		"click #loginDropdownPanel": function() {
			$("#loginDropdown").slideToggle(400);
			if($("#loginDropdown").data("down") === "1") {
				$("#loginDropdown").data("down", "0");
				var rot = 90;
				var loop = setInterval(function() {
					if(rot <= 0) clearInterval(loop);
					else {
						$("#loginDropdownPanel i.fa-arrow-circle-right").rotate(rot);
						rot -= 5;
					}
				}, 8);
			} else {
				$("#loginDropdown").data("down", "1");
				var rot = 0;
				var loop = setInterval(function() {
					if(rot >= 90) clearInterval(loop);
					else {
						$("#loginDropdownPanel i.fa-arrow-circle-right").rotate(rot);
						rot += 5;
					}
				}, 8);
			}
		},
		"click #logout": function() {
			AccountsTemplates.logout();
		}
	});
	Template.registerHelper("extend", function(dict) {
		return $.extend({}, this, dict.hash);
	});
}

if (Meteor.isServer) {
    Meteor.methods({
        "userExists": function(username) {
			check(username, String);
            return !!Meteor.users.findOne({username: username});
        },
	});
}
AccountsTemplates.configure({
 	forbidClientAccountCreation: true
});
AccountsTemplates.removeField("email");
AccountsTemplates.addField({
    _id: "username",
    type: "text",
    required: true,
    func: function(value) {
        if (Meteor.isClient) {
            var self = this;
            self.setValidating(true);
            Meteor.call("userExists", value, function(error, userExists) {
                if (!userExists) {
                    self.setSuccess();
                } else {
                    alert("Username already taken"); //Wait for fix: https://github.com/meteor-useraccounts/core/issues/626
                    self.setError(true);
                }
                self.setValidating(false);
            });
            return;
        }
        return Meteor.call("userExists", value);
    },
    errStr: "Username already taken",
});
AccountsTemplates.addField(AccountsTemplates.removeField("password"));
