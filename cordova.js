sendSMS = function(message, numbers) {
    var options = {
        replaceLineBreaks: true,
        android: {
            intent: "",
        },
    };
    for(var v=0; v<numbers.length; v++) {
        sms.send(numbers[v], message, options);
    }
}

if(Meteor.isCordova) {
    function nfcCallback(nfcEvent) {
        var read = nfc.bytesToString(nfcEvent.tag["ndefMessage"][0]["payload"]);
        if(read.slice(1,3) === "en") read = read.slice(3);
    	Meteor.call("getMedicalData", read, function(error, response) {
    		if(error) {
    			alert(error);
    		} else {
                response = JSON.parse(response);
                $("#receivedData").html("");
                var html = $("<tbody/>");
    			for(var key in response) {
                    html.append(
                        $("<tr/>")
                            .append(
                                $("<td/>").text(key)
                            )
                            .append(
                                $("<td/>").text(response[key])
                            )
                    );
                }
                $("#receivedData").append(html);
    		}
    	});
    }
    Meteor.startup(function() {
        Tracker.autorun(function() {
            if(Meteor.userId()) {
                nfc.addNdefListener(
                    nfcCallback,
//                    () => alert("listening"),
//                    () => alert("could not initialize NFC (turn it on!!)")
                );
            } else {
                nfc.removeNdefListener(nfcCallback);
            }
        });
        Tracker.autorun(function() {
            var data = JSON.stringify(info.all());
            var message = [
                ndef.textRecord(getSendToEMTString(data)),
            ];
            $("#tmp").text(getSendToEMTString(data));
//            alert(JSON.stringify(message));
            nfc.share(
                message,
                () => sendSMS(getEmergencyMessage(), contacts)
//                () => alert("could not share (make sure NFC and Android Beam are on!!)")
            );
//            alert("??");
        });
    });
}
