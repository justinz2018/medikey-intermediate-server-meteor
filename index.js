info = new ReactiveDict("info");
BlueButtonInfo = [
	"Allergies",
	"Current medications",
];
contacts = [];
getEmergencyMessage = function() {
	return `
	Hello,

	`+info.get("fn")+" "+info.get("ln")+" "+`is in an emergency. (S)he listed you as an emergency contact.
	Please call this number for more information.
	`;
}
var defaultShown = [0, 1];

encrypt = function(message) {
	var enc = new JSEncrypt();
	var key = Meteor.settings.public["publicKey"];
	if(key === undefined || key === null || key.length <= 0) {
		key = "-----BEGIN PUBLIC KEY-----\nMIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCpyqlGyggdQGQWS3Wwe2TQKRlQ\n0G28KOQDIOLDuNO1HjoT1LdKbOZuZlSppt1qMqAWPuCgSjNdTMmL2oupR39zjtN3\nrG/u5F6OGzWCa/w31CorfDVE7cpSN0w1Cos1XdcsjU2Hx2silnOjVRNvjalFL1/L\n3Y9wvIZDE1bwSmqL5QIDAQAB\n-----END PUBLIC KEY-----";
	}
	enc.setPublicKey(key);
	return enc.encrypt(message);
}

getSendToEMTString = function(infoStr) {
	return LZString.compress(encrypt(infoStr));
}

if(Meteor.isClient) {
	Template.index.onRendered(function () {
		$("#firstName").editable({
			success: function(response, val) {
				info.set("fn", val);
			}
		});
		$("#lastName").editable({
			success: function(response, val) {
				info.set("ln", val);
			}
		});
		$("#username").editable({
			success: function(response, val) {
				info.set("u", val);
			}
		});
		$("#password").editable({
			success: function(response, val) {
				info.set("p", val);
			}
		});
		var BlueButtonInfo_tmp = {};
		for(var v=0; v<BlueButtonInfo.length; v++) {
			BlueButtonInfo_tmp[v] = BlueButtonInfo[v];
		}
		$("#shownInfo").editable({
			value: defaultShown,
			source: BlueButtonInfo_tmp,
			success: function(response, val) {
				var bitmask = 0;
				for(var v=0; v<val.length; v++) {
					bitmask |= (1<<parseInt(val[v]));
				}
				info.set("s", bitmask);
			}
		});
		$("#contacts").editable({
			success: function(response, val) {
				contacts = val.split("\n");
				for(var v=0; v<contacts.length; v++)
					contacts[v] = contacts[v].trim();
			}
		});
	});
	Template.index.events({
		"click #test": () => {
			var send = getSendToEMTString(JSON.stringify(info.all()));
			$("#tmp").text(send);
			sendSMS(getEmergencyMessage(), contacts);
			Meteor.call("getMedicalData", send, function(error, response) {
				if(error) {
					alert(error);
				} else {
					response = JSON.parse(response);
					$("#receivedData").html("");
					var html = $("<tbody/>");
					for(var key in response) {
						html.append(
							$("<tr/>")
								.append(
									$("<td/>").text(key)
								)
								.append(
									$("<td/>").text(response[key])
								)
						);
					}
					$("#receivedData").append(html);
				}
			});
		}
	});
}
