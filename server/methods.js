TEST_PATIENT_DATA = [
	[				//Allergies
		"Penicillin",
		"Peanuts",
		"Sesame Seeds"
	],
	[				//Current Medications
		"Lipitor",
		"Zebeta",
		"Xanax",
	]
];

Meteor.methods({
	"getMedicalData": function(enc_o) {
		check(enc_o, String);
		if(!Meteor.user()) {
			throw new Meteor.Error(403, "not-authorized"); //Assume all users are EMTs, since non-EMTs don't need user accounts
		}
		var ursa = Meteor.npmRequire("ursa");
		var key = ursa.createPrivateKey(Meteor.settings.private.privateKey);
		try {
			enc = LZString.decompress(enc_o);
			var message = key.decrypt(enc, "base64", "utf8", ursa.RSA_PKCS1_PADDING);
			var data = JSON.parse(message);

			var r = {};
			r["Username"] = data["u"];
			r["Login Successful"] = "p" in data;
			r["First Name"] = data["fn"];
			r["Last Name"] = data["ln"];

			for(var v=0; v<Math.min(BlueButtonInfo.length, TEST_PATIENT_DATA.length); v++) {
				if(!("s" in data) || (data["s"] & (1<<v)) > 0) {
					r[BlueButtonInfo[v]] = TEST_PATIENT_DATA[v];
				}
			}
			return JSON.stringify(r);
		} catch(error) {
//			console.log(error+", RECEIVED: "+enc_o);
			throw new Meteor.Error(400, error+", RECEIVED: "+enc_o);
		}
	},
})
